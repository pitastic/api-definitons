####
#
# makefile to deploy an example aws lambda
#
####

#
# VARIABLES
#

# name of the cloudformation stack to create
STACK_NAME?=pitastic-api-hellopython

# general shortcuts
DOCKER=docker run --rm -ti --env-file ${CURDIR}/.env -v ${CURDIR}:/data -w /data
BASH=${DOCKER} --entrypoint=/bin/bash piaws -c
JQ=${DOCKER} --entrypoint=/usr/bin/jq piaws

#
# TARGETS
#

# build docker image
docker-image:
	docker build -t piaws .

# initialize the aws lambda function
hellopython:
	@echo "execute cloudformation deployment"
	@${BASH} "aws cloudformation deploy \
		--stack-name ${STACK_NAME} --template-file hellopython.yaml \
		--capabilities CAPABILITY_IAM \
		--no-fail-on-empty-changeset"

